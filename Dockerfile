FROM python:3-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ARG TITLE
ARG BACKGROUND

RUN mkdir db

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . .

RUN sed -i -e s#%%%BUILDTAG%%%#$TITLE#g blog/templates/blog/base.html
RUN sed -i -e s#ff9400#$(echo "$BACKGROUND" | cut -c 1-6)#g blog/static/css/blog.css

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
