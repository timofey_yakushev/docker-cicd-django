{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "serviceName" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "labels" -}}
{{ include "selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "selectorLabels" -}}
app.kubernetes.io/name: {{ include "serviceName" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "hostUrl" -}}
{{ .Values.hostUrl }}
{{- end }}

{{- define "dockerImage" -}}
{{ .Values.image.name }}
{{- end }}

{{- define "imagePullSecrets" -}}
{{ .Values.imagePullSecrets }}
{{- end }}